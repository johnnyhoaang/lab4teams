//Johnny Hoang 2036759
public class Cone {
    private double height;
    private double radius;

    public Cone(double height, double radius){
        this.height = height;
        this.radius = radius;
    }
    public double getArea(){
        return Math.PI*this.radius*(this.radius+ Math.sqrt(Math.pow(this.height,2)+Math.pow(this.radius,2)));
    }

    public double getVolume(){
        return Math.PI*(Math.pow(this.radius,2))*(this.height/3);
    }
}
