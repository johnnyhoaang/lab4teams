//Johnny Hoang 2036759
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SphereTest{
    @Test 
    public void getAreaTest(){
        Sphere s = new Sphere(5.0);
        assertEquals(314.1592653589793,s.getSurfaceArea());

    }
    @Test
    public void getVolumeTest(){
        Sphere s = new Sphere(5.0);
        assertEquals(392.6990816987241,s.getVolume());
    }
}
