//Michael Obadia
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class ConeTest {
    Cone c = new Cone(5.3, 2.4);

    @Test
    public void testVolume() {
        assertEquals(c.getArea(), 61.96282784660075);
    }

    @Test
    public void testSurfaceArea() {
        assertEquals(c.getVolume(), 31.968846842929736);
    }
}
